import * as React from 'react';
import {
  Text,
  StatusBar,
  I18nManager
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';

import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from './src/redux/store';


import StackScreens from './src/Navigation/StackScreens'

I18nManager.forceRTL(true)

Text.defaultProps = Text.defaultProps || {};
Text.defaultProps.allowFontScaling = false;

export default function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <NavigationContainer>
          {/* <StatusBar translucent={true} barStyle={'light-content'} backgroundColor={'transparent'} /> */}
          <StackScreens />
        </NavigationContainer >
      </PersistGate>
    </Provider>
  );
}