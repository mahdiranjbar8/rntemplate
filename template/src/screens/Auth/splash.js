import React, { useEffect } from 'react'
import { StyleSheet, Text, View } from 'react-native'

const splash = ({ navigation, route }) => {

    useEffect(() => {
        setTimeout(() => {
            navigation.navigate('BottomTab')
        }, 2000);
        return () => {

        }
    }, [])

    return (
        <View style={styles.main}>
            <Text style={styles.text}>splash</Text>
        </View>
    )
}

export default splash

const styles = StyleSheet.create({
    main: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        fontSize: 20,
        fontFamily: 'Vazir',
        color: '#000'
    },
})
