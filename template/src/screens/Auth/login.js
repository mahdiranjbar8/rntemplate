import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const login = ({ navigation, route }) => {
    return (
        <View style={styles.main}>
            <Text style={styles.text}>login</Text>
        </View>
    )
}

export default login

const styles = StyleSheet.create({
    main: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        fontSize: 20,
        fontFamily: 'Vazir',
        color: '#000'
    },
})
