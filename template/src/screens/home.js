import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const home = ({ navigation, route }) => {
    return (
        <View style={styles.main}>
            <Text style={styles.text}>Home</Text>
        </View>
    )
}

export default home

const styles = StyleSheet.create({
    main: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        fontSize: 20,
        fontFamily: 'Vazir',
        color: '#000'
    },
})
