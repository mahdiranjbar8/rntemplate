export const LOGIN_FLAG = 'LOGIN_FLAG'
export const USER_INFO = 'USER_INFO'
export const TOKEN = 'TOKEN'

export const login = boolean => dispatch => {
    dispatch({
        type: LOGIN_FLAG,
        payload: boolean
    });
};

export const userInfo = info => dispatch => {
    dispatch({
        type: USER_INFO,
        payload: info
    });
};

export const token = info => dispatch => {
    dispatch({
        type: TOKEN,
        payload: info
    });
};