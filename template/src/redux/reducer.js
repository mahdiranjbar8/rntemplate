
import {
    LOGIN_FLAG,
    TOKEN,
    USER_INFO
} from './actions'

const initialState = {
    login: false,
    UserInfo: {},
    token: '',
};

function Reducer(state = initialState, action) {
    switch (action.type) {
        case LOGIN_FLAG:
            return { ...state, login: action.payload };
        case USER_INFO:
            return { ...state, UserInfo: action.payload };
        case TOKEN:
            return { ...state, token: action.payload };
        default:
            return state;
    }
}

export default Reducer;