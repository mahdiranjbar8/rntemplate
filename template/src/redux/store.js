import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Reducer from './reducer';


const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    whitelist: ['DataStore']
};

const rootReducer = combineReducers({
    DataStore: Reducer
});


const persistedReducer = persistReducer(persistConfig, rootReducer);
// const rootReducer = combineReducers({
//     Reducer: persistReducer(persistConfig, Reducer)
// });

export const store = createStore(persistedReducer, applyMiddleware(thunk));
export const persistor = persistStore(store);




//   const store = createStore(persistedReducer, applyMiddleware());
  // createLogger();
//   const persistedStore = persistStore(store);
