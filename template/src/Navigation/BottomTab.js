import React from 'react';
import {
    Image,
    ImageBackground,
    View,
    Dimensions
} from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
// import { useSelector, useDispatch } from 'react-redux';
import { getFocusedRouteNameFromRoute, } from '@react-navigation/native';
import images from '../importedImages/importedImages';

// Stacks
import profileStack from './ProfileStack'
import homeStack from './HomeStack'
import messageStack from '../screens/home'
import myOrdersStack from '../screens/home'

const { width, height } = Dimensions.get('window')


const Tab = createBottomTabNavigator();
const screenOptions = { animationEnabled: false, headerShown: false }


export default function BottomTab({ navigation, route }) {

    return (
        <Tab.Navigator
            initialRouteName='homeStack'
            screenOptions={
                {
                    headerShown: false,
                    "tabBarHideOnKeyboard": true,
                    "tabBarActiveTintColor": "red",
                    "tabBarLabelStyle": {
                        "fontFamily": "Vazir",
                        "marginBottom": 5
                    },
                    "tabBarStyle": [
                        {
                            "display": "flex"
                        },
                        null
                    ]
                }
            }
        >
            <Tab.Screen name="profileStack" component={profileStack}
                initialParams={{ id: 2 }}
                options={{
                    tabBarIcon: props =>
                        <Image source={props.focused ? images.Tab.test : images.Tab.test} style={{ tintColor: props.focused ? 'red' : 'blue', width: 25, height: 25, resizeMode: 'contain' }} />,
                    tabBarLabel: 'حساب کاربری',
                }} />
            <Tab.Screen name="myOrdersStack" component={myOrdersStack}
                initialParams={{ id: 1 }}
                options={{
                    tabBarIcon: props => <Image source={props.focused ? images.Tab.test : images.Tab.test} style={{ tintColor: props.focused ? 'red' : 'blue', width: 25, height: 25, resizeMode: 'contain' }} />,
                    tabBarLabel: 'سفارشات من',
                }} />
            <Tab.Screen name="messageStack" component={messageStack}
                options={{
                    // tabBarVisible: false,
                    tabBarIcon: props => <Image source={props.focused ? images.Tab.test : images.Tab.test} style={{ tintColor: props.focused ? 'red' : 'blue', width: 25, height: 25, resizeMode: 'contain' }} />,
                    tabBarLabel: 'پیام ها',
                }} />

            <Tab.Screen name="homeStack" component={homeStack}
                initialParams={{ id: 0 }}

                options={{
                    tabBarIcon: props => <Image source={props.focused ? images.Tab.test : images.Tab.test} style={{ tintColor: props.focused ? 'red' : 'blue', width: 25, height: 25, resizeMode: 'contain' }} />,
                    tabBarLabel: 'سرویس ها',
                }} />
        </Tab.Navigator >
    );
}