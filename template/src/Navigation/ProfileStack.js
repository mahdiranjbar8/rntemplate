import React, { useEffect } from 'react';
import {
    Text,
    View,
    Dimensions,
    StatusBar,
    I18nManager
} from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';

// Screens

import home from '../screens/home'

const Stack = createStackNavigator();
const ProfileStack = ({ navigation, route }) => {

    const screenOptions = { animationEnabled: false, headerShown: false }

    return (
        <Stack.Navigator initialRouteName='ProfileScreen'>
            <Stack.Screen name="ProfileScreen" component={home} options={screenOptions} />
        </Stack.Navigator>

    );
}

export default ProfileStack;