import React, { useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';

// Screens

import home from '../screens/home'

const Stack = createStackNavigator();
const HomeStack = ({ navigation, route }) => {

    const screenOptions = { animationEnabled: false, headerShown: null }

    return (
        <Stack.Navigator initialRouteName='home'>
            <Stack.Screen name="home" component={home} options={screenOptions} />
        </Stack.Navigator>

    );
}

export default HomeStack;