import * as React from 'react';
import {
    Text,
    View,
    Dimensions,
    StatusBar,
    I18nManager
} from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';

import BottomTab from '../Navigation/BottomTab'

// Login Screens
import login from '../screens/Auth/login'
import splash from '../screens/Auth/splash'

const Stack = createStackNavigator();

export default function StackScreens() {

    const screenOptions = { animationEnabled: false, headerShown: false }

    return (
        <Stack.Navigator initialRouteName='splash'>
            <Stack.Screen name="splash" component={splash} options={screenOptions} />
            <Stack.Screen name="BottomTab" component={BottomTab} options={screenOptions} />
            <Stack.Screen name="login" component={login} options={screenOptions} />
        </Stack.Navigator>

    );
}