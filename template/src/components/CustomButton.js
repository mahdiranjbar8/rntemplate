import React from 'react';
import {
    Text,
    View,
    Image,
    StyleSheet,
    Pressable,
    ActivityIndicator
} from 'react-native';



const CustomButton = ({ disabled = false, loading = false, text, pressAction, color, btnSheet = false }) => {

    return (
        <Pressable disabled={disabled} onPress={pressAction} style={[styles.MainStyle, { opacity: disabled ? 0.5 : 1, backgroundColor: color, height: btnSheet ? 35 : 45, }]}>
            {!loading &&
                <Text style={styles.TextStyle}>{text}</Text>
            }
            {loading &&
                <ActivityIndicator color='white' size='large' style={{ flex: 1, alignSelf: 'center' }} />
            }
        </Pressable>
    );
}


const styles = StyleSheet.create({
    MainStyle: {
        width: '95%',
        marginVertical: 10,
        justifyContent: 'center',
        borderRadius: 5,
        alignSelf: 'center',
        alignItems: 'center'
    },
    TextStyle: {
        fontSize: 15,
        color: 'white',
        fontFamily: 'Vazir',
    }
})
export default CustomButton;