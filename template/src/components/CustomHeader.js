import React from 'react'
import {
    StyleSheet,
    Text,
    View,
    Image,
    Pressable
} from 'react-native'

import images from '../importedImages/importedImages'

const CustomHeader = ({ profile, pressAction = false, screenName = '', home = false, navigation, text, back = true, Drawer = true }) => {
    return (
        <View style={{ overflow: 'hidden', paddingBottom: 2 }}>
            <View style={styles.main}>

                {pressAction != false &&
                    <Text onPress={pressAction} style={{ fontFamily: 'IRANSans', fontSize: 14, color: '#162336' }}>{profile ? 'ویرایش' : 'ثبت'}</Text>
                }
                {(!Drawer && pressAction == false) && <View style={{ width: 20, height: 20 }} />}

                {Drawer &&
                    <Pressable onPress={() => navigation.navigate(screenName)} style={{ padding: 5 }}>
                        <Image source={home ? images.Icons.HomeMenu : images.Icons.Drawer} style={{ width: 20, height: 20, resizeMode: 'contain' }} />
                    </Pressable>
                }
                <Text numberOfLines={1} adjustsFontSizeToFit style={styles.text}>{text}</Text>
                {!back && <View style={{ width: 20, height: 20 }} />}
                {back &&
                    <Pressable onPress={() => navigation.goBack()} style={{ padding: 2 }}>
                        <Image source={images.Icons.LeftBlue} style={{ width: 25, height: 25, resizeMode: 'contain' }} />
                    </Pressable>
                }
            </View>
        </View>

    )
}

export default CustomHeader

const styles = StyleSheet.create({
    main: {
        paddingHorizontal: 15,
        height: 50,
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        shadowColor: '#000',
        shadowOffset: {
            width: 1,
            height: 2
        },
        shadowOpacity: 0.4,
        shadowRadius: 3,
        elevation: 5,
    },
    text: {
        color: '#162336',
        fontSize: 16,
        width: '70%',
        textAlign: 'center',
        fontFamily: 'Vazir'
    }
})
