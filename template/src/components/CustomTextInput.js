import React, { useState } from 'react';
import {
    View,
    Text,
    TextInput,
    StyleSheet,
    Image
} from 'react-native';


const CustomTextInput = ({ error = undefined, height = undefined, handleBlur = undefined, maxlength = undefined, secureTextEntry = false, small = false, numeric = false, left = false, wallet = false, onChangeText, value, image, label, showImage = false, multiLine = false }) => {


    return (
        <View style={[styles.container, { height: multiLine ? 90 : height }]}>
            <View style={[styles.labelContainer, { left: small ? 15 : 30, backgroundColor: wallet ? 'white' : '#183573', }]}>
                <Text style={{ fontSize: 12, fontFamily: 'Vazir', color: wallet ? '#152F66' : 'white' }}>{label}</Text>
            </View>
            <View style={[styles.ChildView, { borderColor: wallet ? '#152F66' : typeof error == 'undefined' ? "#ccc" : '#CC2211', height: wallet ? 40 : 44, flexDirection: left ? 'row-reverse' : 'row' }]} >
                {showImage &&
                    <Image
                        source={image}
                        style={styles.BackImageStyle}
                    />
                }
                <TextInput
                    error={error}
                    onBlur={handleBlur}
                    maxLength={maxlength}
                    autoCompleteType='off'
                    secureTextEntry={secureTextEntry}
                    value={value}
                    textAlignVertical={multiLine ? 'top' : 'center'}
                    multiline={multiLine}
                    style={[styles.InputStyle, { textAlign: numeric ? 'left' : 'right', width: showImage ? '87%' : '100%', color: wallet ? 'black' : 'white' }]}
                    onChangeText={onChangeText}
                    keyboardType={numeric ? 'numeric' : 'default'}
                />
            </View>
            {error &&
                <Text style={styles.errorText} >{error}</Text>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        position: 'relative',
        alignItems: 'center',
        marginTop: 20,
    },
    errorText: {
        alignSelf: 'flex-start',
        fontFamily: 'Vazir',
        color: 'red',
        paddingHorizontal: 20,
        fontSize: 10,
    },
    BackImageStyle: {
        width: 16,
        alignSelf: 'center',
        height: 16,
        opacity: 0.75,
        marginHorizontal: 4,
        resizeMode: 'contain'
    },
    labelContainer: {
        position: 'absolute',
        top: -14,
        padding: 2,
        zIndex: 50,
    },
    ChildView: {
        flex: 1,
        borderWidth: 1,
        width: '95%',
        borderRadius: 6,
        paddingHorizontal: 10,
        justifyContent: 'flex-start'
    },
    InputStyle: {
        fontFamily: 'Vazir',
        height: '75%',
        paddingVertical: 0,
        opacity: 0.75,
        alignSelf: 'center'
    }
})

export default CustomTextInput;